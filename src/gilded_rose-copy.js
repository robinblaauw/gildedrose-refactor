class Item {
  constructor(name, sellIn, quality){
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }
}

class Shop {
  constructor(items=[]){
    this.items = items;
  }
  updateQuality() {
    // takes current item in the shop and updates quality, then goes to next item in shop
    for (let i = 0; i < this.items.length; i++) {
      // Check for the 2 types that increase in quality instead of decrease in quality as time goes on
      // The 2 types are Aged Brie and Backstage passes to a TAFKAL80ETC concert
      if (this.items[i].name != 'Aged Brie' && this.items[i].name != 'Backstage passes to a TAFKAL80ETC concert') {
        // quality can not be lower than 0
        if (this.items[i].quality > 0) {
          // Check for legendary item, since it does not decrease in quality
          if (this.items[i].name != 'Sulfuras, Hand of Ragnaros' && this.items[i].name != 'Conjured Mana Cake') {
            // decrease quality by 1
            this.items[i].quality = this.items[i].quality - 1;
          } else if(this.items[i].name === 'Conjured Mana Cake') {
            this.items[i].quality = this.items[i].quality - 2;
          }
        }
      } else {
        if (this.items[i].quality < 50) {
          this.items[i].quality = this.items[i].quality + 1;
          if (this.items[i].name == 'Backstage passes to a TAFKAL80ETC concert') {
            if (this.items[i].sellIn < 11) {
              if (this.items[i].quality < 50) {
                this.items[i].quality = this.items[i].quality + 1;
              }
            }
            if (this.items[i].sellIn < 6) {
              if (this.items[i].quality < 50) {
                this.items[i].quality = this.items[i].quality + 1;
              }
            }
          }
        }
      }
      // All items except for the legendary item decreases it's sell in property.
      if (this.items[i].name != 'Sulfuras, Hand of Ragnaros') {
        this.items[i].sellIn = this.items[i].sellIn - 1;
      }
      // 
      if (this.items[i].sellIn < 0) {
        if (this.items[i].name != 'Aged Brie') {
          if (this.items[i].name != 'Backstage passes to a TAFKAL80ETC concert') {
            if (this.items[i].quality > 0) {
              if (this.items[i].name != 'Sulfuras, Hand of Ragnaros' && this.items[i].name != 'Conjured Mana Cake') {
                this.items[i].quality = this.items[i].quality - 1;
              } else if(this.items[i].name === 'Conjured Mana Cake') {
                this.items[i].quality = this.items[i].quality - 2;
              }
            }
          } else {
            this.items[i].quality = this.items[i].quality - this.items[i].quality;
          }
        } else {
          if (this.items[i].quality < 50) {
            this.items[i].quality = this.items[i].quality + 1;
          }
        }
      }
    }

    return this.items;
  }
}

module.exports = {
  Item,
  Shop
}
