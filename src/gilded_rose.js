class Item {
  constructor(name, sellIn, quality){
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }
}

class Shop {
  constructor(items=[]){
    this.items = items;
  }

  getQualityDecrease(sellIn) {
    if (sellIn >= 0) return 1;
    return 2;
  }

  updateQuality() { 
    for (const item of this.items) {    
      // If the item is not legendary the sellIn property of the item decreased by 1
      if(item.name !== "Sulfuras, Hand of Ragnaros" && item.name !== "Thunderfury, Blessed Blade of the Windseeker") {
        item.sellIn -= 1;
      }
      switch(item.name) {
        // Backstage passes gain quality up until the day of the concert, then it loses all it's quality.
        case "Backstage passes to a TAFKAL80ETC concert":
          if(item.sellIn >= 11) {
            item.quality ++;
          }
          else if(item.sellIn >= 5) {
            item.quality += 2;
          }
          else if(item.sellIn >= 0) {
            item.quality += 3;
          }
          else {
            item.quality = 0;
          }
          break;

        // Agex brie gains quality when time passes
        case "Aged Brie":
          item.quality ++;
          break;

        // Conjured items lose 2 quality each day instead of 1 of normal items
        case "Conjured Mana Cake":
          item.quality -= 2 * this.getQualityDecrease(item.sellIn);
          break;

        // Legendary items never lose quality
        case "Sulfuras, Hand of Ragnaros" || "Thunderfury, Blessed Blade of the Windseeker":
            break;

        // Regular shop items decrease 1 quality per day, after the sellIn period is over it starts to be cumulative
        default:
          item.quality -= this.getQualityDecrease(item.sellIn);
      }

      // Final check to not drop quality below 0
      if(item.quality < 0) {
        item.quality = 0;
      }
      // Final check to not let quality go over 50
      if(item.quality > 50 && item.name !== "Sulfuras, Hand of Ragnaros" && item.name !== "Thunderfury, Blessed Blade of the Windseeker") {
        item.quality = 50;
      }
    }

    return this.items;
  }
}

module.exports = {
  Item,
  Shop
}
