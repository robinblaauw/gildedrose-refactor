const {Shop, Item} = require("../src/gilded_rose");

describe("Item: Gilded Rose", function() {
  //Arrange
  const items = [
    new Item("Conjured Mana Cake", 2, 18)
  ]
  const gildedRose = new Shop(items);

  it("Should decrease twice per day and four when past sellIn", function() {
    //Act
    for (let day = 0; day < 2; day++) {
      gildedRose.updateQuality();
    }

    //Assert
    const expected = {name: "Conjured Mana Cake", sellIn: 0, quality: 14}
    expect(items[0]).toMatchObject(expected);

    //Act
    gildedRose.updateQuality();

    //Assert
    const expectedPastSellIn = {name: "Conjured Mana Cake", sellIn: -1, quality: 10}
    expect(items[0]).toMatchObject(expectedPastSellIn);
  });
});
